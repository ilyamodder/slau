package matrix;

import java.io.File;
import java.util.List;
import java.util.Random;

/**
 * Created by ilya on 14.04.15.
 */
public class Calculator implements ISlauCalculator {
    private double[][] matrix;
    private double[] solution;
    @Override
    public void calculate(File f) {
        calculate();
    }

    @Override
    public void calculate(String path) {
        calculate(new File(path));
    }

    @Override
    public void calculate(double[][] m) {
        this.matrix = m;
        calculate();
    }

    private void calculate() {
        triangleView();
        double[] solutions = findSolutions();
    }

    public double[] getSolution() {
        return solution;
    }

    private void triangleView() {
        final int bound = Math.min(matrix.length, matrix[0].length);
        Thread[] threads = new Thread[bound];
        for (int k = 1; k < bound; k++) {
            final int finalK = k;
            threads[k] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = finalK; j < bound; j++) {
                        double m = matrix[j][finalK - 1] / matrix[finalK - 1][finalK - 1];
                        for (int i = 0; i < bound; i++) {
                            matrix[j][i] = matrix[j][i] - m * matrix[finalK - 1][i];
                        }
                    }
                }
            });
            threads[k].start();
        }

        try {
            for (int i=1; i<bound; i++) {
                threads[i].join();
            }
        } catch (InterruptedException e) {

        }

        for (int i = bound; i < matrix.length; i++) {
            for (int j=0; j<matrix[i].length; j++) {
                matrix[i][j] = 0;
            }
        }
    }

    private double[] findSolutions() {
        int i = getNotZeroString();
        Random r = new Random();
        if (i == -1)
            for (int j = 0; j < solution.length; j++)
                solution[j] = r.nextDouble();
        else {
            for (int j = solution.length; j >= matrix[0].length-i; j--)
                solution[j] = r.nextDouble();
            for (; i >= 0; i--)
                for (int j = solution.length-1; j > i; j--)
                    solution[i] += solution[j];
        }
        return solution;
    }

    private int getNotZeroString(){
        for (int i = matrix.length-1; i >=0; i--)
            for (int j = 0; j < matrix[0].length; j++)
                if (matrix[i][j] != 0) return i;
        return -1;
    }
}
