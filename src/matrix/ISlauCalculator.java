package matrix;

import java.io.File;

/**
 * Created by ilya on 14.04.15.
 */
public interface ISlauCalculator {
    void calculate(File f);
    void calculate(String path);
    void calculate(double[][] m);
}
